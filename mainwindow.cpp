#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore/QCoreApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->exit, SIGNAL(triggered()), this, SLOT(closeapp()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(closeapp()));

    connect(ui->aboutApp, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->butRasch, SIGNAL(clicked()), this, SLOT(fuel()));
    connect(ui->butRaschVes, SIGNAL(clicked()), this, SLOT(ves()));
    connect(ui->comboBrake, SIGNAL(currentIndexChanged(int)), ui->comboWind, SLOT(setCurrentIndex(int)));
    connect(ui->pbLbs, SIGNAL(clicked()), this, SLOT(lbs()));
    connect(ui->pbKg, SIGNAL(clicked()), this, SLOT(kg()));


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::fuel()
{
    int S = ui->spinBoxS->value();
    int Salt = ui->spinBoxSalt->value();
    //float Tpsum = ((float)S+(float)Salt)/270; //Время полета для общего топлива
    float Tzap = (float)Salt/270;//Время полета до запасного
    float Tp = (float)S/270; //Время полета до ап
    int Hh = (int)Tp;
    int Mm = (int)((Tp- Hh)*60);
    QTime n;
    n.setHMS(Hh,Mm,00,00);
    int Gpol = (Tp*170*0.03)+(Tp*170);
    int Gzap = (Tzap*170)+85;
    int Gf = Gpol + Gzap;
    ui->lineEditGp->setText(QString::number(Gpol));//Вывод топлива на полет
    ui->lineEditGz->setText(QString::number(Gzap));//Вывод топлива до запасного
    ui->lineEditG->setText(QString::number(Gf));//Вывод общего топлива
    ui->lineEditG_2->setText(QString::number(Gf));//Вывод общего топлива для загрузки
    ui->timeEdit->setTime(n);//Вывод времени полёта
    /*
    QGraphicsScene * scen = new QGraphicsScene();
    QPixmap * pix = new QPixmap();
    pix->load("gr.png");
    scen->addPixmap(*pix);
    ui->Mnemo->setScene(scen);//Mnemo - компонент QGraphicsView

    */

}

void MainWindow::ves()
{

    if (ui->cbBort->currentIndex() == 0){
        float Gpust = 2436.57;//вес пустого кг
        int Gsl = 18;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(Gvzl));
        float CG = (((1050.80+10.31+(((float)Gfuel/0.4536)/4.92)+25.84+25.84+((((float)ui->spinZagr->value()/0.4536)*186.85)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*224.82)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*262.79)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((((float)ui->spinTks->value()*9.2)*195.77)/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number(Centr));
        ui->lineCG->setText(QString::number(CG));//вывод плеча CG location(для графика)

    }
    else if (ui->cbBort->currentIndex() == 1){
        float Gpust = 2444.95;//вес пустого кг
        int Gsl = 18;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(Gvzl));
        float CG = (((1051.97+10.31+(((float)Gfuel/0.4536)/4.92)+25.84+25.84+((((float)ui->spinZagr->value()/0.4536)*186.85)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*224.82)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*262.79)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((((float)ui->spinTks->value()*9.2)*195.77)/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number(Centr));
        ui->lineCG->setText(QString::number(CG));

    }
    else if (ui->cbBort->currentIndex() == 2){
        float Gpust = 2441.37;//вес пустого кг
        int Gsl = 18;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(Gvzl));
        float CG = (((1030.60+10.31+(((float)Gfuel/0.4536)/4.92)+25.84+25.84+((((float)ui->spinZagr->value()/0.4536)*186.85)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*224.82)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*262.79)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((((float)ui->spinTks->value()*9.2)*195.77)/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number(Centr));
        ui->lineCG->setText(QString::number(CG));

    }
}

void MainWindow::lbs()//Фунты->Кг
{
  int kg1;
  int lbs1 = ui->lineLbs1->text().toInt();
  ui->lineKg1->clear();
  kg1 =  round(lbs1*0.45359237);
  ui->lineKg1->setText(QString::number(kg1));
  ui->lineKg2->clear();
  ui->lineLbs2->clear();
}

void MainWindow::kg()//Кг->Фунты
{
  int lbs2;
  int kg2 = ui->lineKg2->text().toInt();
  ui->lineLbs2->clear();
  lbs2 = round(kg2*2.2046223302272);
  ui->lineLbs2->setText(QString::number(lbs2));
  ui->lineKg1->clear();
  ui->lineLbs1->clear();
}

void MainWindow::about()
{
    QMessageBox msgBox;
    QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
    msgBox.setStyleSheet("background-color: rgb(0, 0, 0);");
    msgBox.setText(tr("Расчёты для Cessna 208B \n ООО СКОЛ \n Версия 1.0\n Автор: Зуев М.Г.\n pilot40@gmail.com \n Лицензия: GPL v.3"));
    okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
    msgBox.exec();
}

void MainWindow::closeapp()
{
    close();
}

void MainWindow::setOrientation(ScreenOrientation orientation)
{
#if defined(Q_OS_SYMBIAN)
    // If the version of Qt on the device is < 4.7.2, that attribute won't work
    if (orientation != ScreenOrientationAuto) {
        const QStringList v = QString::fromAscii(qVersion()).split(QLatin1Char('.'));
        if (v.count() == 3 && (v.at(0).toInt() << 16 | v.at(1).toInt() << 8 | v.at(2).toInt()) < 0x040702) {
            qWarning("Screen orientation locking only supported with Qt 4.7.2 and above");
            return;
        }
    }
#endif // Q_OS_SYMBIAN

    Qt::WidgetAttribute attribute;
    switch (orientation) {
#if (QT_VERSION < QT_VERSION_CHECK(4, 7, 2)) || (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    // Qt < 4.7.2 does not yet have the Qt::WA_*Orientation attributes
    case ScreenOrientationLockPortrait:
        attribute = static_cast<Qt::WidgetAttribute>(128);
        break;
    case ScreenOrientationLockLandscape:
        attribute = static_cast<Qt::WidgetAttribute>(129);
        break;
    default:
    case ScreenOrientationAuto:
        attribute = static_cast<Qt::WidgetAttribute>(130);
        break;
#else // QT_VERSION < 0x040702
    case ScreenOrientationLockPortrait:
        attribute = Qt::WA_LockPortraitOrientation;
        break;
    case ScreenOrientationLockLandscape:
        attribute = Qt::WA_LockLandscapeOrientation;
        break;
    default:
    case ScreenOrientationAuto:
        attribute = Qt::WA_AutoOrientation;
        break;
#endif // QT_VERSION < 0x040702
    };
    setAttribute(attribute, true);
}

void MainWindow::showExpanded()
{
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_SIMULATOR)
    showFullScreen();
#elif defined(Q_WS_MAEMO_5)
    showMaximized();
#else
    show();
#endif
}
